var result;

var xPos, yPos;
var multi = 5;

//colors
var faceC;
var mouthC;
var tongueF;
var tongueS;

//sound
var mic;
var fft;

function setup() {
  createCanvas(800, 600);

  xPos = width/2;
  yPos = height/3;

  faceC = color(194, 247, 211);
  mouthC = color(46, 128, 68);
  tongueF = color(51, 198, 90);
  tongueS = color(98, 222, 124);

  mic = new p5.AudioIn();
  mic.start();
  fft = new p5.FFT();
  mic.connect(fft);
}

function draw() {
  background(faceC);
  result = audio();
  face(xPos, yPos);
  console.log(result);
  result = 0;
}

function audio() {
  //get volume
  var vol = mic.getLevel();
  var mappedvol = map(vol, 0, 1, 0,100);

  return mappedvol;
}

function face(x, y) {
  strokeWeight(3);
  if (result > 3) {
    mouth(x-40, y+100, int(result*multi));
  } else {
    closedMouth(x, y);
  }
  eyes(x, y);
}

function mouth(x,  y,  total) {
  //fill mouth
  background(mouthC);
  tongue(x+40, y-100, int(result*multi));
  teeth(x+40, y-100);
  stroke(0);
  fill(faceC);
  //draw mouth
  beginShape();
  vertex(-2, -2);
  vertex(-2, height+2);
  vertex(width+2, height+2);
  vertex(width+2, -2);
  beginContour();
  vertex(x, y);
  bezierVertex(x+30, y+10, x+50, y+10, x+80, y);
  bezierVertex(x+120, y-20, x+140, y-20+total, x+90, y+20+total);
  bezierVertex(x+60, y+40+total, x+20, y+40+total, x-10, y+20+total);
  bezierVertex(x-60, y-20+total, x-40, y-20, x, y);
  endContour();
  endShape(CLOSE);
}

function closedMouth( x,  y) {
  stroke(0);
  noFill();
  bezier(x-70, y+93, x-20, y+110, x+20, y+110, x+70, y+93);
}

function tongue( x,  y,  total) {
  stroke(tongueS);
  fill(tongueF);
  ellipse(x, y+137+total, 100, 50);
}

function teeth( x,  y) {
  stroke(0);
  fill(255);
  rect(x-100, y+70, 200, 50);
}

function eyes( x,  y) {
  //blink
  if (frameCount % 200 < 10) {
    closedEyes(x, y);
  } else {
    fill(0);
    ellipse(x-width/4, y, 30, 50);
    ellipse(x+width/4, y, 30, 50);
  }
}

function closedEyes( x,  y) {
  noFill();
  arc(x-width/4, y, 45, 45, 0, PI);
  arc(x+width/4, y, 45, 45, 0, PI);
}
