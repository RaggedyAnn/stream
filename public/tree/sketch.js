/*
Made by Ann Karring

Tree Generator - modification for twitch

My inspiration for this generator was this example: "P_2_2_4_01" from website: http://www.generative-gestaltung.de/code
*/

var rotc=5;
var rots=18;
var sWeight=8;

var tall;
var place;


function setup() {
  createCanvas(windowWidth,windowHeight);
  //noLoop();

  frameRate(0.5);
}

function draw() {
  background(131,235,221);
  stroke(35,28,124);
  strokeWeight(sWeight);
  push();
  translate(width/2,height);
  tree(0);
  pop();
  noStroke();
  fill(35,28,124);
  textSize(60);
  textFont('Courier New');
  text("I'll be right back", width/18,(height/10)*9);
  noFill();
}

//the tall variable is to make sure that the tree stops getting more branches after a specific amount of times
function tree (tall){
  var branchP=-height/10;
  if (tall < 12){
    line(0,0,0,branchP);
    translate(0,branchP);
    rotate(radians(random(-rotc,rotc)));

     if (random(10) < 7){ // branching
      scale(0.8);

      rotate(radians(rots));

      push();
      tree(tall + 1);
      pop();

      rotate(radians(-rots*2));
      push();
      tree(tall + 1);
      pop();

      } else {
      tree(tall);
    }
  if(tall>4) {
      leaf(branchP);
    }
  }
}

//adding some very simple pink leaves in various colours of pink
function leaf(place) {
  noStroke();
  fill(255,int(random(59,180)),int(random(213,239)));
  ellipse(20,random(place),25,12);
  ellipse(-20,random(place),25,12);
  stroke(35,28,124);
}
